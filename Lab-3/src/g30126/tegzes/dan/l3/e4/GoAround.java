package g30126.tegzes.dan.l3.e4;

import becker.robots.*;

public class GoAround
{
   public static void main(String[] args)
   {
      City cluj = new City();

      Wall blockAve0 = new Wall(cluj, 1, 1, Direction.NORTH);
      Wall blockAve01 = new Wall(cluj, 1, 2, Direction.NORTH);
      
      Wall blockAve1 = new Wall(cluj, 1, 1, Direction.WEST);
      Wall blockAve11 = new Wall(cluj, 2, 1, Direction.WEST);
      
      Wall blockAve2 = new Wall(cluj, 2, 2, Direction.EAST);    
      Wall blockAve21 = new Wall(cluj, 1, 2, Direction.EAST);
      
      Wall blockAve3 = new Wall(cluj, 2, 1, Direction.SOUTH);
      Wall blockAve31 = new Wall(cluj, 2, 2, Direction.SOUTH);
      
      Robot robo = new Robot(cluj, 0, 2, Direction.WEST);
      
      robo.move();
      robo.move();
      robo.turnLeft();
      robo.move();
      robo.move();
      robo.move();
      robo.turnLeft();
      robo.move();
      robo.move();
      robo.move();
      robo.turnLeft();
      robo.move();
      robo.move();
      robo.move();   
      robo.turnLeft();
      robo.move();
   }
} 
