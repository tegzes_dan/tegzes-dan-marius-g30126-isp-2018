package g30126.tegzes.dan.l3.testalgo1;

public class Solution {
    public int solution(int[] A) {

        boolean solutia = true;

        for(int i = 0; i < A.length; i++){ //parcurgem sirul
        	solutia = true;
            for(int j = 0; j < A.length; j++) // parcurgem sirul iar
                if(A[i] == A[j] && i != j){ // conditia ca elemntele sa fie egale, dar pozitiile diferite
                	solutia = false; //ia val false pentru a elimina perechile
                    break; 
            }
            if(solutia) // daca se gaseste o solutie (un nr fara pereche)
                return A[i]; //returneaza numarul

        }
        return 0;
    }
}
