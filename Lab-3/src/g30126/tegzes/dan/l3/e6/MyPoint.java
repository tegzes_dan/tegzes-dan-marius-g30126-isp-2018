package g30126.tegzes.dan.l3.e6;

public class MyPoint {
	//Two instance variables x and y
int x;
int y;

 //A �no-argument� constructor 
 //that construct a point at (0, 0)
public	MyPoint(){
	x=0;
	y=0;
}

//A constructor 
//that constructs a point with the given x and y
public MyPoint(int x, int y){
		this.x=x;
		this.y=y;
	}

//getter and setter for x	
	int getX(){
		return x;
	}
	void setX(int x){
		this.x=x;
	}
	
//getter and setter for y	
	int getY(){
		return y;
	}
	void setY(int y){
		this.y=y;
	}
	
	//method to set both x and y
	void setXY(int x,int y){
		this.x=x;
		this.y=y;
	}
	
	//method that returns 
	//a string description in the format "(x,y)"
	@Override
    public String toString() {
        return "(" + x + ", "+ y + ")";
    }
	
	//method that return distance
	public double distance(int x, int y){
		int xd = this.x - x;
		int yd = this.y - y;
		return Math.sqrt(xd * xd + yd * yd);
	}
	
	//overloaded distance that returns
	//distance from this point to the given MyPoint
	public double distance(MyPoint p2) {
        return distance(p2.getX(), p2.getY());
    }	
    }

