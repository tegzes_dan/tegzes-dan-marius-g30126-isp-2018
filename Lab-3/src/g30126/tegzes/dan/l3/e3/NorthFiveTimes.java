package g30126.tegzes.dan.l3.e3;

import becker.robots.*;

public class NorthFiveTimes
{
   public static void main(String[] args)
   {
      // Set up the initial situation
      City cluj = new City(); 
      Robot mark = new Robot(cluj, 1, 1, Direction.NORTH);
      
      // mark goes north five times
      mark.move();
      mark.move();
      mark.move();
      mark.move();
      mark.move();
      mark.turnLeft();     // start turning around as two turning lefts
      mark.turnLeft();    // finished turning around
      mark.move();
      mark.move();
      mark.move();
      mark.move();
      mark.move();
      mark.move(); //back at the starting point
   }
} 