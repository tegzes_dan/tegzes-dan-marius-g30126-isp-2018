package g30126.tegzes.dan.l3.e5;

import becker.robots.*;

public class NewsPaper
{
   public static void main(String[] args)
   {
      // Set up the initial situation
      City ny = new City();
      Thing parcel = new Thing(ny, 2, 3);

      Wall blockAve0 = new Wall(ny, 1, 2, Direction.NORTH);
      Wall blockAve01 = new Wall(ny, 1, 3, Direction.NORTH);
      
      Wall blockAve1 = new Wall(ny, 1, 2, Direction.WEST);
      Wall blockAve11 = new Wall(ny, 2, 2, Direction.WEST);
      
      Wall blockAve2 = new Wall(ny, 1, 3, Direction.EAST);    
      
      Wall blockAve3 = new Wall(ny, 2, 2, Direction.SOUTH);
      Wall blockAve31 = new Wall(ny, 1, 3, Direction.SOUTH);
      
      Robot karel = new Robot(ny, 1, 3, Direction.WEST); //karel is in bed
      
      karel.move(); //karel wakes up
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.move();
      karel.pickThing(); //karel takes the paper
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
      karel.move(); //karel return to bed
    
   }
} 