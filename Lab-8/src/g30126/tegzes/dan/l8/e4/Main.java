package g30126.tegzes.dan.l8.e4;

public class Main {
	public static void main(String[] args) throws Exception{
		
		CarManager cm = new CarManager();
		
		Car car1 = cm.createCar("Porche",80000);
		Car car2 = cm.createCar("Mercedes",55000);
		cm.addCar(car1, "car1.dat");
		cm.addCar(car2, "car2.dat");
		
		Car car3 = cm.createCar("Range Rover",65000);
		cm.addCar(car3, "car3.dat");
		System.out.println(car2.toString());
	}
}