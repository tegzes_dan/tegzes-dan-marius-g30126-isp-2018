package g30126.tegzes.dan.l8.e1;

public class NumberException extends Exception{
	    int n;
	    public NumberException(int n,String msg) {
	          super(msg);
	          this.n = n;
	    }

	    int getN(){
	          return n;
	    }
	}

