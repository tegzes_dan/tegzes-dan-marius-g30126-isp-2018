package g30126.tegzes.dan.l8.e1;

class Cofee{
	private int temp;
    private int conc;
    private static int n;
    
    Cofee(int t,int c){temp = t;
    n++;
    conc = c;
    }
    int getTemp(){return temp;}
    int getConc(){return conc;}
    int getN(){return n;}
    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+"]" + "nr = " + n;}
}
