package g30126.tegzes.dan.l10.e3;

public class Count extends Thread {
	
	static int nr=0;
	String n;
    Thread t;
    
    public Count(String n)
    {
    	super(n);
    }

  public void run(){
	  System.out.println("Firul "+getName()+" a intrat in metoda run()");
	   for(int i=0;i<100;i++){
        	nr++;
              System.out.println(getName() + " nr = "+nr);
	   }
	  try
      {                
            if (t!=null) 
            	t.join();
            System.out.println("Firul "+getName()+" executa operatie.");
            Thread.sleep(10000);
            System.out.println("Firul "+getName()+" a terminat operatia.");
      }
      catch(Exception e)
	  {e.printStackTrace();} 
	  
  }

  public static void main(String[] args) {
        Count c1 = new Count("Count1");
        Count c2 = new Count("Count2");

        c1.start();
        c2.start();    
  	}	
}


