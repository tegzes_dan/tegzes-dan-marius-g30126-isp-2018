package g30126.tegzes.dan.l10.e6;

public class Time {
	
	private int t;
	private boolean go;
	
	Time(){
		t=0;
		go=false;
	}
	
	public void resetT()
	{
		t=0;
	}
	
	public int getT()
	{
		return this.t;
	}
	
	synchronized public void increment()
	{
		try
		{
			while(go==false)
			{
				wait();
			}
			t++;
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	synchronized public void startceas()
	{
		go=true;
		notify();
	}
	
	public void stopceas()
	{
		go=false;
	}
	
	public boolean getGo()
	{
		return this.go;
	}
}
