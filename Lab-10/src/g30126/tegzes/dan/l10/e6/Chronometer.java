package g30126.tegzes.dan.l10.e6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.*;


public class Chronometer extends JFrame{

	private Time t= new Time();
	
	private static class Time1 extends Thread
	{
		private Time t;
		
		public Time1(String name,Time t){
			super(name);
			this.t=t;
		}
		
		 public void run()
		 {
			 while(true) {
				 t.increment();
				 try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			 }
		 }
	}
	
	
	JButton a;
	JButton b;
	JTextField count;
	JLabel nr;
	int k=0;
	
	Chronometer(Time t)
	{	
		this.t=t;
        setTitle("Chronometer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300,200);
        setVisible(true);
	}
	
	 public void init(){
		 
         this.setLayout(null);
         int width=80;int height = 20;
         
         nr = new JLabel("Timp:");
         nr.setBounds(10, 60, width, height);
         
         count = new JTextField(String.valueOf(k));
         count.setBounds(50,50,width, height);
         
         a = new JButton("Start/Stop");
         a.setBounds(10,110,width*2, height);
         
         b = new JButton("Reset");
         b.setBounds(10,130,width*2, height);
         
         a.addActionListener(new TratareButon1());
         b.addActionListener(new TratareButon());
         
         add(nr);add(count);add(a);add(b);
	 }
	 
	 public class TratareButon1 implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				if(t.getGo()==true)
				{
					t.stopceas();
				}
				else
				{
					t.startceas();
				}

			}	
		 }
		 
	 
	 public class TratareButon implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			t.resetT();
		}	
	 }
	 
	 
	 public static void main(String[] args) 
	 {

		 Time ti=new Time();
        new Time1("Ceas",ti).start();
        Chronometer ceas=new Chronometer(ti);
        while(true) {
        	ceas.count.setText(String.valueOf(ti.getT()));
        }
	 }

}