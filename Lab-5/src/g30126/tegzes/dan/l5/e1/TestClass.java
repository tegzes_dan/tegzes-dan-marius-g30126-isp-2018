package g30126.tegzes.dan.l5.e1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestClass {
	 @Test
	    public void forCircle(){
		 Shape c=new Circle(3,"red",true);     
		 assertEquals("A Circle with radius= 3.0 and color red and filled",c.toString());;
		 assertEquals(28.2735,c.getArea(),0.1);;
		 assertEquals(18.849,c.getPerimeter(),0.1);;
	    }
	 @Test
	    public void forRectangle(){
		 Shape r=new Rectangle(3,6,"red",true);     
		 assertEquals("A Rectangle with width= 3.0 and length= 6.0 and filled",r.toString());;
		 assertEquals(18,r.getArea(),0.1);;
		 assertEquals(18,r.getPerimeter(),0.1);;
	    }
	 @Test
	    public void forSquare(){
		 Shape s=new Square(4,"red",true);     
		 assertEquals("A Square with side= 4.0 and filled, which is a subclass of A Rectangle with width= 4.0 and length= 4.0 and filled",s.toString());;
		 assertEquals(16,s.getArea(),0.1);;
		 assertEquals(16,s.getPerimeter(),0.1);;
	    }
	 
}
