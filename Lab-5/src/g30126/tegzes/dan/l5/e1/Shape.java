package g30126.tegzes.dan.l5.e1;

public abstract class Shape {
	protected String color="red";
	protected Boolean filled=true;
	
	Shape(){
		color="red";
		filled=true;
	}
	
	Shape(String color, boolean filled){
		this.color=color;
		this.filled=filled;
	}
	
	public String getColor(){
		return color;
	}
	
	public void setColor(String color){
		this.color=color;
	}
	
	public boolean isFilled(){
		return filled;
	}
	
	public void setFilled(boolean filled){
		this.filled=filled;
	}
	
	 abstract double getArea();
	
	public abstract double getPerimeter();
		
	
	public String toString(){
		if (filled==true)
			return "A Shape with color of " + color + " and " + "filled.";
		else
			return "A Shape with color of " + color + " and " + "not filled.";
	}
	
	public static void main(String[] args){
		Shape[] s = new Shape[3];
    	
    	s[0]=new Circle(3,"red",true);
    	s[1]=new Rectangle(3,6,"red",true); 
    	s[2]=new Square(4,"red",true);

    	for(int i=0;i<3;i++)
    	{
    		System.out.println(s[i].getArea());
    		System.out.println(s[i].getPerimeter());
    		System.out.println(s[i].toString());
    	}
    	
	}
}

