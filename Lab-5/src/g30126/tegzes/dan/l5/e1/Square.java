package g30126.tegzes.dan.l5.e1;


public class Square extends Rectangle{

	Square() {
		super.width=1.0;
		super.length=1.0;
	}
	
	public Square(double side){
		super.width=side;
		super.length=side;
	}
	
	public Square(double side, String color,boolean filled){
		super.width=side;
		super.length=side;
		super.color=color;
		super.filled=filled;
	}
	
	public double getSide(){
		return width;
	}
	
	public void setSide(double side){
		super.width = side;
		super.length = side;	
		
	}
	
	public void setWidth(double side){
		super.width=side;
		super.length=side;
		
	}
	
	public void setLength(double side){
		super.length=side;
		super.width=side;
	}
	
	@Override
	public String toString(){
		if(super.filled =true)
		return "A Square with side= " + super.width + " and filled, which is a subclass of " + super.toString();
		else
			return "A Square with side= " + super.width + " and not filled, which is a subclass of " + super.toString();
	}
	
}

