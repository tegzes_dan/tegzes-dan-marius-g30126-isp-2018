package g30126.tegzes.dan.l5.e1;


public class Circle extends Shape{
	 protected double radius;
	 
	 Circle(){
			radius=3.0;
			color="red";
		}
	 
	 public Circle(double radius){
		 this.radius=radius;
	 }
	 
	 public Circle(double radius,String color, boolean filled){
		 this.radius=radius;
		 this.color=color;
		 this.filled=filled;
	 }
	 
	 double getRadius(){
		 return radius;
	 }

	 void setRadius(double radius){
		 this.radius=radius;
	 }
	 
	  double getArea(){
		 return radius*radius*Math.PI;
	 }
	 
	 public double getPerimeter(){
		 
		 return 2*radius*Math.PI;
	 }
	 
	 @Override 
	 public String toString(){
		 if(filled = true)
		 return "A Circle with radius= " + radius + " and color "+ color + " and filled";
		 else
			 return "A Circle with radius= " + radius + " and color "+ color + " and not filled"; 
	 }
	 public static void main(String[] args){
			Circle c = new Circle();
			c.getArea();
	}
}