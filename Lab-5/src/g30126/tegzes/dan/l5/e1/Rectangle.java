package g30126.tegzes.dan.l5.e1;


public class Rectangle extends Shape{

	protected double width=1.0;
	protected double length=1.0;
	
	public	Rectangle(){
		length=1.0;
		width=1.0;
	}
	
	Rectangle(double width, double length){
	this.width=width;
	this.length=length;
}

	Rectangle(double width, double length,String color, boolean filled){
		this.width=width;
		this.length=length;
		super.color=color;
		super.filled=filled;
	}
	
	public double getWidth(){
		return width;
	}
	
	public void setWidth(double width){
		this.width=width;
	}
	
	public double getLength(){
		return length;
	}
	
	public void setLength(double length){
		this.length=length;
	}
	
	public double getArea(){
		return length*width;
	}
	
	public double getPerimeter(){
		return 2*length + 2*width;
	}
	
	@Override
	public String toString(){
		if(filled =true)
		return "A Rectangle with width= " + width + " and length= " + length + " and filled"; 
		else
			return "A Rectangle with width= " + width + " and length= " + length + " and not filled";
	}
	
}

