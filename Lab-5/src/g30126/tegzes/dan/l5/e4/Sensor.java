package g30126.tegzes.dan.l5.e4;

public abstract class Sensor {
	
	private String location;
	
	abstract int readValue();
	
	
	public String getLocation()
	{
		return this.location;
	}
}
