package g30126.tegzes.dan.l5.e2;

public class ProxyImage implements Image{ 

	private RealImage realImage;
	private RotatedImage rotatedImage;
	private String fileName;
	boolean rotated;

	
	public ProxyImage(String fileName,boolean rotated){
		this.fileName= fileName;
		this.rotated=rotated;
	}
	
	@Override
	public void display(){
		if(rotated ==false){
		if(realImage == null){
			realImage = new RealImage(fileName);
		}
		realImage.display();
		} else {
	if(rotatedImage == null){
		rotatedImage = new RotatedImage(fileName);
		}
	rotatedImage.display();
		}
	}
}
