package g30126.tegzes.dan.l5.e3;

import java.util.Random;


public class TemperatureSensor extends Sensor{
	public int readValue()
	{
		Random r = new Random();
		int t=r.nextInt(100);
		return t;
	}
}
