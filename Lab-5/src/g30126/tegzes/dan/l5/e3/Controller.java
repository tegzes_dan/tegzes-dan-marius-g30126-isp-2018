package g30126.tegzes.dan.l5.e3;

public class Controller {
	public static void control()
	{
		TemperatureSensor a = new TemperatureSensor();
		LightSensor b = new LightSensor();
		int x,y;
		int i;
		for(i=0;i<20;i++)
		{
			try
			{
				Thread.sleep(1000);
				i++;
				x=a.readValue();
				y=b.readValue();
				System.out.println("TemperatureSensor value="+x);
				System.out.println("LightSensor value="+y);
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public static void main(String [] args) throws InterruptedException
	{
		control();
	}
}
