package g30126.tegzes.dan.l9.e2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;

 
public class ClickMe implements ActionListener {
	 int counter = 0;
	
	 JLabel counterLabel;
	 JButton countUpButton;

	public JPanel createPanel() {
	
		JPanel clickMePanel = new JPanel();
	
		countUpButton = new JButton ("Click Me");
		countUpButton.addActionListener(this);
		clickMePanel.add(countUpButton);
	
		counterLabel = new JLabel("" + counter);
		clickMePanel.add(counterLabel);
		
		return clickMePanel;
	}

	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == countUpButton) {
			counter++;
			counterLabel.setText("" + counter);
		}
		
	}

	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setTitle("Click Me");
		f.setSize(new Dimension(200, 100));
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ClickMe clickMe = new ClickMe();
		JPanel clickMePanel = clickMe.createPanel(); 
			
		Container contentPane = f.getContentPane();
		contentPane.add(clickMePanel, BorderLayout.CENTER);
			
		f.setVisible(true);

	}

}