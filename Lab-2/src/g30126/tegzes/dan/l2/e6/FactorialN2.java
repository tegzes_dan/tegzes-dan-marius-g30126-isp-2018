package g30126.tegzes.dan.l2.e6;

import java.util.Scanner;

public class FactorialN2 {
	
	static int factorial(int n)
    {
        int result;
       if(n==0 || n==1)
    	   return 1;

       result = factorial(n-1) * n;
       return result;
    }
	
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Give n: ");
	    int n = in.nextInt();
		in.close();
		System.out.println("The factorial is: " + factorial(n));
		//factorial(n);
	}	
}
