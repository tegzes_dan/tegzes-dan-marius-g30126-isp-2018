package g30126.tegzes.dan.l2.e6;

import java.util.Scanner;

public class FactorialN1 {
	static void factorial(int n) { //non-recursive method
	    int f = 1;
	    for(int i=1;i<=n;i++) {
	        f *= i;
	    }
	    //return f;
	    System.out.println("The factorial is: " + f);
	}
	
	public static void main(String[] args){
	Scanner in = new Scanner(System.in);
	System.out.println("Give n: ");
    int n = in.nextInt();
	in.close();
	//System.out.println("The factorial is: ");
	factorial(n);
	
}	
}