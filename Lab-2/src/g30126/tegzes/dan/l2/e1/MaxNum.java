package g30126.tegzes.dan.l2.e1;

import java.util.Scanner;
import java.lang.Math;

public class MaxNum {
	public static void main(String[] args){
	      Scanner in = new Scanner(System.in);
	      System.out.println("Please write first number: ");
	      int x = in.nextInt();
	      System.out.println("Please write second number: ");
	      int y = in.nextInt();
	      int max = Math.max(x,y);
	      System.out.println("The max is: " + max);
	      in.close();
	     

	  }
}
