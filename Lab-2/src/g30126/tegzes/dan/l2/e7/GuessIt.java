package g30126.tegzes.dan.l2.e7;

import java.util.Random;
import java.util.Scanner;

public class GuessIt {
	public static void main(String[] args){
		
		Random rand = new Random();
		int numToGuess = rand.nextInt(100);
		int numOfTries = 0;
		Scanner in = new Scanner(System.in);
		int guess;
		boolean win = false;
		System.out.println("Guess a number between 1 and 100! ");
		while(!win){
			
			System.out.println("Type your guess: ");
			guess = in.nextInt();
			numOfTries++;
			
			if(guess == numToGuess){
				System.out.println("You guessed it!");
			} else if(guess < numToGuess){
				System.out.println("Wrong answer, your number it too low!");
			} else if (guess > numToGuess){
				System.out.println("Wrong answer, your number it too high!");
			}
			if(numOfTries == 3){
				System.out.println("You lost");
				break;
			}
		}
		in.close();

	}
}
