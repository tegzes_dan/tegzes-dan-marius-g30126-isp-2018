package g30126.tegzes.dan.l2.e3;

import java.util.Scanner;

public class PrimeNumbs {
	 public static void main(String[] args) {

		 Scanner in = new Scanner(System.in);
		  System.out.println("Write number A: ");
	      int a = in.nextInt();
	      System.out.println("Write number B: ");
	      int b = in.nextInt();
	      in.close();
	        while (a < b) {
	            boolean flag = false;
	            for(int i = 2; i <= a/2; ++i) { 
	                if(a % i == 0) { //conditia pentru numere neprime
	                    flag = true; //cand un numar e neprim, se declanseaza flag-ul care il elimina
	                    break;
	                }
	            }
	            if (!flag) //toate numerele pentru care flag-ul nu s-a activat, adica cele prime
	                System.out.print(a + " ");	          
	            ++a;	            
	        }	        
	    }	 	 
}
