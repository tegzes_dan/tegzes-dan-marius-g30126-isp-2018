package g30126.tegzes.dan.l2.e2;
import java.util.Scanner;

public class PrintNumberInWord2 {

	public static void main(String[] args){
	      Scanner in = new Scanner(System.in);
	      System.out.println("Write a number: ");
	      int number = in.nextInt();
	      in.close();
	      
	      String wnumber;
	      switch (number) {
          case 1:  wnumber = "ONE";
                   break;
          case 2:  wnumber = "TWO";
                   break;
          case 3:  wnumber = "THREE";
                   break;
          case 4:  wnumber = "FOUR";
                   break;
          case 5:  wnumber = "FIVE";
                   break;
          case 6:  wnumber = "SIX";
                   break;
          case 7:  wnumber = "SEVEN";
                   break;
          case 8:  wnumber = "EIGHT";
                   break;
          case 9:  wnumber = "NINE";
                   break;
          default: wnumber = "OTHER";
                   break;
      }
      System.out.println(wnumber);
	}     
}
