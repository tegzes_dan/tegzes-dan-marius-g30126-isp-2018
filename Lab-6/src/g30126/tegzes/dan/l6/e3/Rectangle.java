package g30126.tegzes.dan.l6.e3;


import java.awt.*;

public class Rectangle implements Shape{

    private int length;
    private Color color;
    private int x;
    private int y;
    private String id;
    private boolean fill;

    public Rectangle(Color color,int x,int y, int length,String id,boolean fill) {
    	this.color = color;
    	this.x = x;
    	this.y = y;
    	this.id = id;
    	this.fill=fill;
    	this.length = length;
    }
    
    public void setColor(Color color){
    	this.color = color;
    }
    
    public Color getColor(){
    	return color;
    }
       
    
    public String getId(){
    	return id;
    }
  
    public int getLength() {
        return length;
    }
    
    public int getX(){
    	return x;
    }
    
    public int getY(){
    	return y;
    }
    
    public boolean getFill(){
    	return fill;
    }
    
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+this.length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),length,length);
        if(fill==true){
        	g.fillRect(getX(), getY(), length, length);
        }
    }
}

