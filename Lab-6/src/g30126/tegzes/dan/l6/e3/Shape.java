package g30126.tegzes.dan.l6.e3;

import java.awt.*;

public interface Shape {
   
    public String getId();
    
    public int getX();
    
    public int getY();
    
    public boolean getFill();
    
    public Color getColor() ;

    public void setColor(Color color);
    public  void draw(Graphics g);
}
