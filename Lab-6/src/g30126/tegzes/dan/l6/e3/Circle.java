package g30126.tegzes.dan.l6.e3;

import java.awt.*;

public class Circle implements Shape{

    private int radius;
    private Color color;
    private int x;
    private int y;
    private String id;
    private boolean fill;

    public Circle(Color color,int x,int y, int radius,String id,boolean fill) {
    	this.color = color;
    	this.x = x;
    	this.y = y;
    	this.id = id;
    	this.fill=fill;
        this.radius = radius;
    }
    
    public void setColor(Color color){
    	this.color = color;
    }
    
    public Color getColor(){
    	return color;
    }
    
    public int getRadius() {
        return radius;
    }

    public int getX(){
    	return x;
    }
    
    public int getY(){
    	return y;
    }
    
    public boolean getFill(){
    	return fill;
    }
    
    public String getId(){
    	return id;
    }
    
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
        if(fill==true){
        	g.fillRect(getX(), getY(), radius, radius);
        }
    }
}

