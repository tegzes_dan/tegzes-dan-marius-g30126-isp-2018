package g30126.tegzes.dan.l6.e5;

import java.awt.*;
import java.awt.Graphics;


public interface Shape {
	
	public int getLength();
	public int getWidth();
    public abstract void draw(Graphics g);
    public int getLastNumBricks();
    public int getNumbricks();
	public int getBricksInLayer();
    
}