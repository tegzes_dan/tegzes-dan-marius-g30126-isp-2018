package g30126.tegzes.dan.l6.e5;


import java.awt.Color;
import java.awt.Graphics;

public class Pyramid implements Shape {

    private int length;
    private int width;
	private int lastnumbricks;
	private int numbricks;
	
	public Pyramid(int length,int width,int numbricks){
		this.length=length;
		this.width=width;
		this.numbricks=numbricks;
	}
	
    
    public int getLength(){
    	return this.length;
    }
    
    public int getWidth(){
    	return this.width;
    }
	
	public int getNumbricks()
	{
		return this.numbricks;
	}
	
	public int getLastNumBricks()
	{
		return this.lastnumbricks;
	}
	
	public int getBricksInLayer()
	{
		int i=1;
		int s=getNumbricks();
		if(s-i>=0)
		{
			while(s-i>=0)
			{
				s=s-i;
				i++;
			}
			i--;
			s=s+i;
			numbricks=numbricks-i;
			if(i==lastnumbricks)
				return i-1;
			else
				return i;
		}
		else
			return -1;
	}
	
	public void draw(Graphics g)
	{
		int s=getBricksInLayer();
		lastnumbricks=s;
		int x=300,y=300;
		int lastx=x;
		int i;
		while(s!=-1)
		{
			for(i=1;i<=s;i++)
			{
				g.drawRect(x,y,getLength(),getWidth());
				x=x+length;
			}
			y=y-width;
			x=lastx+length/2;
			lastx=x;
			s=getBricksInLayer();
			lastnumbricks=s;
		}
	}
	
}
