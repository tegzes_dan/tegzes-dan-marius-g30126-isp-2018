package g30126.tegzes.dan.l6.e1;

import java.awt.*;

public abstract class Shape {

    private Color color;
public int x;
public int y;
private String id;
public boolean fill;

    public Shape(Color color, int x,int y,String id,boolean fill) {
        this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
    }
    
    public String getId(){
    	return id;
    }
    
    public int getX(){
    	return x;
    }
    
    public int getY(){
    	return y;
    }
    
    public boolean getFill(){
    	return fill;
    }
    
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
}
