package g30126.tegzes.dan.l6.e1;

import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color,int x,int y, int radius,String id,boolean fill) {
        super(color,x,y,id,fill);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public int getX(){
    	return x;
    }
    
    public int getY(){
    	return y;
    }
    
    public boolean getFill(){
    	return fill;
    }
    
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
        if(fill==true){
        	g.fillRect(getX(), getY(), radius, radius);
        }
    }
}
