package g30126.tegzes.dan.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;

    public Rectangle(Color color,int x, int y, int length,String id,boolean fill) {
        super(color,x,y,id,fill);
        this.length = length;
    }
    public int getLength() {
        return length;
    }
    
    public int getX(){
    	return x;
    }
    
    public int getY(){
    	return y;
    }
    
    public boolean getFill(){
    	return fill;
    }
    
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+this.length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),length,length);
        if(fill==true){
        	g.fillRect(getX(), getY(), length, length);
        }
    }
}
