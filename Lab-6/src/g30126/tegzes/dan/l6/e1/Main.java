package g30126.tegzes.dan.l6.e1;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90,80,100,"111",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100,200,134,"m222",true);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLUE, 150,66,55,"333",false);
        b1.addShape(s3);
        b1.deleteById("m222");
    }
}
