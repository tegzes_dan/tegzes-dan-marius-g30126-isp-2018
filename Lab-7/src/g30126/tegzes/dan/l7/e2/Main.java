package g30126.tegzes.dan.l7.e2;

public class Main {
	public static void main(String args[])
	{
		BankAccount a = new BankAccount("ut1",4000);
		BankAccount b = new BankAccount("ut2",2000);
		BankAccount c = new BankAccount("ut3",3000);	
		Bank banca=new Bank();
		banca.addAccount(a.getOwner(),a.getBalance());
		banca.addAccount(b.getOwner(),b.getBalance());
		banca.addAccount(c.getOwner(),c.getBalance());
		banca.printAccounts();
	}

}
