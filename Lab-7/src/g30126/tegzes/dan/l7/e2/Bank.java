package g30126.tegzes.dan.l7.e2;

import java.util.ArrayList;
import java.util.Collections;

public class Bank {

	private ArrayList <BankAccount> accounts;
	
	public Bank()
	{
		accounts = new ArrayList<>();
	}
	
	public void addAccount(String owner,double balance)
	{
		BankAccount b = new BankAccount(owner,balance);
        accounts.add(b);
	}
	
	public void printAccounts()
	{
		Collections.sort(accounts);
		for(BankAccount a:accounts)
		{
			System.out.println(a.getOwner()+" "+a.getBalance());	
		}
	}
	
	public void printAccounts(double minBalance,double maxBalance)
	{
		for(BankAccount b:accounts)
		{
			if(b.getBalance()>minBalance && b.getBalance()<maxBalance)
				System.out.println(this.accounts);
		}
	}
	
	public BankAccount getAccount(String owner)
	{
		for(BankAccount b:accounts)
		{
			if(b.getOwner()==owner)
				return b;
		}
		return null;
	}
	
	public void getAllAccount()
	{
		
		String nume;
		for(BankAccount a:accounts)
		{
			nume=a.getOwner();
			for(BankAccount b:accounts)
			{
				if(a.getOwner().equals(b.getOwner()))
				{
					nume=a.getOwner();
				}
			}
			System.out.println(getAccount(nume));	
		}
	}
}
