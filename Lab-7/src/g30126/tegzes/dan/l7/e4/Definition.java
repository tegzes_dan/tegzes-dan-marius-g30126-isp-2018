package g30126.tegzes.dan.l7.e4;

public class Definition {
	
	public String description;
	
	public Definition(String description)
	{
		this.description=description;
	}

	
	public String getDefinition()
	{
		return this.description;
	}
	
	public void setDefinition(String description)
	{
		this.description=description;
	}
}