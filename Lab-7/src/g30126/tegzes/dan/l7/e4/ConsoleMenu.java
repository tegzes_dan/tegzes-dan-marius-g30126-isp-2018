package g30126.tegzes.dan.l7.e4;

import java.util.Scanner;

public class ConsoleMenu {
	public static void main(String[] args)
	{
		int k=100;
		String cuvant,definitie;
		Word a;
		Definition b;
		Dictionary d = new Dictionary();
		while(k!=0)
		{
			System.out.println("0.iesire 1.adauga cuvant 2.primeste definitia 3.primeste toate cuvintele 4.primeste toate definitiile.");
			Scanner in = new Scanner(System.in);
			k=Integer.parseInt(in.nextLine());
			if(k==1)
			{
				System.out.println("Care este cuvantul?");
				cuvant=in.nextLine();
				a=new Word(cuvant);
				System.out.println("Care este definitia?");
				definitie=in.nextLine();
				b=new Definition(definitie);
				d.addWord(a, b);
			}
			if(k==2)
			{
				System.out.println("Care este cuvantul?");
				cuvant=in.nextLine();
				a=new Word(cuvant);
				System.out.println(d.getDefinition(a));
			}
			if(k==3)
			{
				d.getAllWords();
			}
			if(k==4)
			{
				d.getAllDefinitions();
			}
		}
	}
}
