package g30126.tegzes.dan.l7.e4;

public class Word {
	
	private String nume;
	
	public Word(String nume)
	{
		this.nume=nume;
	}
	
	public String getWord()
	{
		return this.nume;
	}
	
	public void setWord(String nume)
	{
		this.nume=nume;
	}
	
	
}
