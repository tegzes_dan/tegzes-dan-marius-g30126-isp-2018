package g30126.tegzes.dan.l4.e3;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestCircle {
	 @Test
	    public void shouldGetArea(){
	        Circle c = new Circle(3.2371);     
	        assertEquals(32.921, c.getArea() ,0.01);
	    }
}
