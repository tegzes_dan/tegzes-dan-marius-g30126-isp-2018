package g30126.tegzes.dan.l4.e3;

public class Circle {
private double radius = 1.0;
private String color = "red";

public Circle(){
	this.radius = 10.0;
	this.color = "red";
}

public Circle(double radius){
	this.radius = radius;
	this.color = color;
}

double getRadius(){
	return radius;
}

double getArea(){
	return radius*radius*Math.PI;
}

}
