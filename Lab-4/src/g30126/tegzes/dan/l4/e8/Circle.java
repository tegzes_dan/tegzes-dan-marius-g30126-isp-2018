package g30126.tegzes.dan.l4.e8;

public class Circle extends Shape{
 private double radius;
 private String color;
 
 Circle(){
		radius=1.0;
		color="red";
	}
 
 public Circle(double radius){
	 this.radius=radius;
 }
 
 public Circle(double radius,String color, boolean filled){
	 this.radius=radius;
	 this.color=color;
 }
 
 double getRadius(){
	 return radius;
 }

 void setRadius(double radius){
	 this.radius=radius;
 }
 
 double getArea(){
	 return radius*radius*Math.PI;
 }
 
 double getPerimeter(){
	 
	 return 2*radius*Math.PI;
 }
 
 @Override 
 public String toString(){
	 return "A Circle with radius=" + radius + ", which is a subclass of "+ super.toString();
 }
}
