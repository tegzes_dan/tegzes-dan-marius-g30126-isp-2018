package g30126.tegzes.dan.l4.e8;

public class Square extends Rectangle{

	Square() {
		super.width=1.0;
		super.length=1.0;
	}
	
	public Square(double side){
		super.width=side;
		super.length=side;
	}
	
	public Square(double side, String color,boolean filled){
		super.width=side;
		super.length=side;
		super.color=color;
		super.filled=filled;
	}
	
	public double getSide(){
		return width;
	}
	
	public void setSide(double side){
		super.width = side;
		super.length = side;	
		
	}
	
	public void setWidth(double side){
		super.width=side;
		super.length=side;
		
	}
	
	public void setLength(double side){
		super.length=side;
		super.width=side;
	}
	
	@Override
	public String toString(){
		return "A Square with side=" + super.width + ", which is a subclass of " + super.toString();
	}
	
}
