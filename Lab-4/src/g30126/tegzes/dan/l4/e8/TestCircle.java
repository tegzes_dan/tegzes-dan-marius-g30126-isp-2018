package g30126.tegzes.dan.l4.e8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestCircle {
	 @Test
	    public void shouldGetArea(){
		 Circle c = new Circle(3.2);    
		 assertEquals(32.153,c.getArea(),0.1);;
	    }
	 

	 	 @Test
	 	    public void shouldGetPerimeter(){
	 		Circle c = new Circle(3.2);     
	 		 assertEquals(20.09,c.getPerimeter(),0.1);;
	 	    }
	 
	
	 	 @Test
	 	    public void shouldGetString(){
	 		Circle c = new Circle(3.2,"red",false);
	 		 assertEquals("A Circle with radius=3.2, which is a subclass of A Shape with color of red and filled.",c.toString());
	 	    }
	 }

