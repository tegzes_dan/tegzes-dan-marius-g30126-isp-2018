package g30126.tegzes.dan.l4.e4;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestAuthor {
	 @Test
	    public void shouldGetAuthor(){
		 Author a = new Author("Ion","ion@ion.com",'m');	    
		 assertEquals("Ion(m) at ion@ion.com", a.toString());
	    }
}
