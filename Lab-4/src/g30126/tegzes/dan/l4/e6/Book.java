package g30126.tegzes.dan.l4.e6;

import g30126.tegzes.dan.l4.e4.Author;


public class Book {
private String name;
private Author[] authors;
private double price;
private int qtyInStock = 0;

public Book(String name, Author[] authors,double price){
	this.name=name;
	this.authors=authors;
	this.price=price;
	
	}

public Book(String name, Author[] authors,double price, int qtyInStock){
	this.name=name;
	this.authors=authors;
	this.price=price;
	this.qtyInStock=qtyInStock;
	}

String getName(){
	return name;
}

 Author[] getAuthors(){
	return authors;
}

double getPrice(){
	return price;
}

void setPrice(double price){
	this.price=price;
}

int getQtyInStock(){
	return qtyInStock;
}

void setQtyInStock(int qtyInStock){
	this.qtyInStock=qtyInStock;
}

public String toString(){
	return "book name" + name + "by " + authors.length;
}

void printAuthors(){
	for(Author a:authors){
		System.out.println(a.getName());
		}
	}

}

