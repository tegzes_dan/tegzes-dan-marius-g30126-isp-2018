package g30126.tegzes.dan.l4.e5;
import g30126.tegzes.dan.l4.e4.Author;

public class Book {
private String name;
private Author author;
private double price;
private int qtyInStock = 0;

public Book(String name, Author author,double price){
	this.name=name;
	this.author=author;
	this.price=price;
	
	}

public Book(String name, Author author,double price, int qtyInStock){
	this.name=name;
	this.author=author;
	this.price=price;
	this.qtyInStock=qtyInStock;
	}

String getName(){
	return name;
}

 Author getAuthor(){
	return author;
}

double getPrice(){
	return price;
}

void setPrice(double price){
	this.price=price;
}

int getQtyInStock(){
	return qtyInStock;
}

void setQtyInStock(int qtyInStock){
	this.qtyInStock=qtyInStock;
}

public String toString(){
	return name + " by " + author;
}
public static void main(String[] args){
	Author a = new Author("Ion","Ion@ion.com",'m');
	Book b = new Book("Povesti",a, 30.0);
	System.out.println(b.toString());
	}
}
