package g30126.tegzes.dan.l4.e5;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import g30126.tegzes.dan.l4.e4.Author;

public class TestBook {
	@Test
    public void shouldGetBook(){
		Author a = new Author("Ion","Ion@ion.com",'m');
		Book b = new Book("Povesti",a, 30.0);    
	 assertEquals("Povesti by Ion(m) at Ion@ion.com", b.toString());
    }
}
