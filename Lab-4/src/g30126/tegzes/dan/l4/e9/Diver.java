package g30126.tegzes.dan.l4.e9;

import becker.robots.*;

public class Diver extends Robot{
	
	public Diver(City aCity, int aStreet,int anAvenue, Direction aDirection){
		super(aCity,aStreet,anAvenue,aDirection);
	}

public void turnRight(){
	this.turnLeft();
	this.turnLeft();
	this.turnLeft();

	} 

public void doFlip(){
	this.turnRight();
	this.turnRight();
	this.turnRight();

	
	}

public void dive(){
	this.move();
	this.turnRight();
	this.move();
	this.turnRight();
	this.move();
	this.doFlip();
	this.turnRight();
	this.move();
	this.move();
	this.move();
	this.turnLeft();
	this.turnLeft();
	}

}

	  

