package g30126.tegzes.dan.l4.e9;

import becker.robots.*;

public class Robot1 {
	public static void main(String[] args){
		   City cluj = new City(); 
		   Diver karel = new Diver(cluj, 1, 1, Direction.NORTH);
		   
		   Wall blockAve0 = new Wall(cluj, 2, 1, Direction.NORTH);
		   
		   Wall blockAve1 = new Wall(cluj, 2, 0, Direction.EAST);
		   Wall blockAve2 = new Wall(cluj, 3, 0, Direction.EAST);
		   Wall blockAve3 = new Wall(cluj, 4, 0, Direction.EAST);
		   
		   Wall blockAve4 = new Wall(cluj, 4, 1, Direction.WEST);
		   
		   Wall blockAve5 = new Wall(cluj, 4, 2, Direction.SOUTH);
		   Wall blockAve6 = new Wall(cluj, 4, 1, Direction.SOUTH);
		   
		   Wall blockAve7 = new Wall(cluj, 4, 2, Direction.EAST);
		   
		   karel.dive();
	}
}
