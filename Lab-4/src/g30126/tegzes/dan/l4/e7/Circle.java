package g30126.tegzes.dan.l4.e7;

public class Circle {
private double radius = 1.0;
private String color = "red";

public Circle(){
	this.radius = 10.0;
	this.color = "red";
}

public Circle(double radius){
	this.radius = radius;
	
}

double getRadius(){
	return radius;
}

double getArea(){
	return radius*radius*Math.PI;
}

public static void main(String[] args)
{
	 Circle c1=new Circle();
	 Circle c2=new Circle(2);
	 double r=c2.getRadius();
	 System.out.println("Radius c1:"+c1.getRadius());
	 System.out.println("Radius c2:"+r);
	 System.out.println("Area:"+c2.getArea()); 
}

}

