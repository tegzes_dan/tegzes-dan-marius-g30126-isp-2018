package g30126.tegzes.dan.l4.e7;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestCylinder {
	 @Test
	    public void shouldGetVolume(){
		 Cylinder c = new Cylinder(10,4);     
	        assertEquals(1256, c.getVolume() ,0.9);
	    }
}

